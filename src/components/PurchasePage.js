import React,{useState} from 'react';
import { useHistory } from 'react-router-dom';
import Header from './Header';
import PackClassCircle from './PackClassCircle';
import { useAuth } from "../context/auth";
import response from '../response';
import '../styles/purchasePage.css';

function PurchasePage(props){
  const {packId} = props.match.params;
  const packList = response.data.pack_list;
  const [selectedPackItem] = packList.filter(pack => pack.pack_id === packId);

  let history = useHistory();

  const [subtotal] = useState(parseFloat(selectedPackItem.pack_price).toFixed(2))
  const [GST] = useState((parseFloat(selectedPackItem.pack_price) * 0.07).toFixed(2))
  const [discount,setDiscount] = useState(null)
  const [grandTotal,setGrandTotal] = useState(parseFloat(subtotal)+parseFloat(GST))
  const [promoCode,setPromoCode] = useState('');
  const [hidePromoCode,setHidePromoCode] = useState(false);
  const [isValidPromoCode,setIsValidPromoCode] = useState(false);
  const [isPurchased, setIsPurchased] = useState(false);

  const { loggedInUser } = useAuth();

  const handlePurchaseClick = () => {
    setHidePromoCode(true)
    setIsPurchased(true)
  }

  const handlePromoCodeChange = (e) => {
    setPromoCode(e.target.value)
    if(e.target.value === 'code-test'){
      setIsValidPromoCode(true);
    }
  }

  const handlePromoCodeApply = () => {
    setDiscount(subtotal * 0.07);
    setGrandTotal(subtotal);
  }

  return(
    <>
      <Header/>
      <div style={{padding: '20px',height: '100%',backgroundColor: '#ededed'}}>
        <h3 className='title'>
        {
          isPurchased ?
            'THANK YOU!\nYOU HAVE SUCCESSFULLY PURCHASED A CLASS PACK!'
          :
          'CLASS PACK PURCHASE PREVIEW'
        }
        </h3>
        <div style={{backgroundColor: 'white',padding: '10px'}}>
          <h4>You have selected:</h4>
          <div style={{display: 'flex'}}>
            <PackClassCircle alias={selectedPackItem.pack_alias} />
            <div style={{marginLeft: '10px'}}>
              <p className='pack-title'>{selectedPackItem.pack_name}</p>
              {
                selectedPackItem.pack_alias.includes('unlimited') ?
                  <span className='newbie-note'>Billed every month</span>
                :
                  selectedPackItem.newbie_note &&
                    <span className='newbie-note'>{selectedPackItem.newbie_note}</span>
              }
            </div>
            {
              selectedPackItem.pack_alias.includes('unlimited') ?
                <div style={{display: 'flex',flexDirection: 'column',flexGrow: 1,textAlign: 'right'}}>
                  <p className='selected-pack-price'>
                    ${parseFloat(selectedPackItem.pack_price).toFixed(2)}
                  </p>
                  <span className='selected-metered-text'>per month</span>
                </div>
                :
                <p className='selected-pack-price'>
                  ${parseFloat(selectedPackItem.pack_price).toFixed(2)}
                </p>
            }
          </div>
          {
            (loggedInUser.hasPromoCode && !hidePromoCode) &&
              <div className="promo-code-section" style={{display: 'flex',margin: '20px 0'}}>
                <div className="promo-code-field">
                  <input className="promo-code-input" placeholder='Your promo code' onChange={handlePromoCodeChange}/>
                  {
                    promoCode.length > 0 && isValidPromoCode &&
                      <i className="fa fa-check-circle" aria-hidden="true" style={{fontSize: '0.8rem',color: 'green'}}></i>
                  }
                  {
                    promoCode.length > 0 && !isValidPromoCode &&
                      <i className="fa fa-times-circle" aria-hidden="true" style={{fontSize: '0.8rem',color: 'red'}}></i>
                  }
                </div>
                <button
                  onClick={handlePromoCodeApply}
                  style={{backgroundColor: '#00BCD4',padding: '0.6rem 1rem',color: 'white'}}>
                  <b>APPLY</b>
                </button>
              </div>
          }

          <hr className='divider'/>

          <section className='purchase-details'>
            <dl>
              <dt>Subtotal</dt>
              <dd>${subtotal}</dd>

              <dt>GST</dt>
              <dd>${GST}</dd>
              {
                discount && isValidPromoCode &&
                <>
                  <dt><b>Discount</b></dt>
                  <dd><b>-${discount}</b></dd>
                </>
              }

              <dt style={{color: 'teal',fontWeight: 700}}>Grand Total</dt>
              <dd style={{color: 'teal',fontWeight: 700}}>${grandTotal}</dd>
            </dl>
          </section>
        </div>

        <p style={{fontSize: '0.9rem',marginTop: '2rem',marginBottom: '1.5rem'}}>
          Please read all <a href='#'>Terms & Conditions</a> before purchasing your YM Class or Class Pack.
        </p>
        {
          !isPurchased &&
          <div style={{display: 'flex',justifyContent: 'space-between'}}>
            <a href="#" onClick={()=>history.goBack()}>{'<- Back'}</a>
            <button className='btn-pay' onClick={handlePurchaseClick}><b>PAY NOW</b></button>
          </div>
        }
      </div>
    </>
  )
}

export default PurchasePage;