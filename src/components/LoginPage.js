import React, {useState,useRef} from 'react';
import { Redirect } from "react-router-dom";
import { useAuth } from "../context/auth";
import '../styles/login.css';

const nonPromoCodeUser = {
  email: 'code-test@gmail.com',
  hasPromoCode: false,
  password: 'nopromocode'
}
const promoCodeUser = {
  email: 'code-test@gmail.com',
  hasPromoCode: true,
  password: 'promocode'
}

function LoginPage(props){
  const [isLoggedIn, setIsLoggedIn] = useState(
                                      !!localStorage.getItem('user') ?
                                        JSON.parse(localStorage.getItem('user')).isLoggedIn
                                        :
                                        false
                                      );
  const [errorMessage, setErrorMessage] = useState('');
  const { setLoggedInUser } = useAuth();

  const emailRef = useRef(null)
  const passwordRef = useRef(null)

  const handleSubmit = (event) => {
    event.preventDefault();
    const email = emailRef.current.value;
    const password = passwordRef.current.value;
    if(email === 'code-test@gmail.com'){
      if(password === nonPromoCodeUser.password){
        setLoggedInUser({email: nonPromoCodeUser.email,hasPromoCode: nonPromoCodeUser.hasPromoCode,isLoggedIn: true})
      }else if(password === promoCodeUser.password){
        setLoggedInUser({email: promoCodeUser.email,hasPromoCode: promoCodeUser.hasPromoCode,isLoggedIn: true})
      }
      setIsLoggedIn(true);
    }else{
      setErrorMessage("Email and password didn't match")
    }
  }

  if (isLoggedIn) {
    return <Redirect to='/' />;
  }

  return(
    <div className="login-page">
      <div className="login-form">
        <p className="">Member Login</p>
        <form onSubmit={handleSubmit}>
          <input type="email" name="email" ref={emailRef} placeholder="Email" autoFocus/>
          <input type="password" name="password" ref={passwordRef} placeholder="Password"/>
          { errorMessage && <p style={{color: 'red'}}>{errorMessage}</p>}
          <input type="submit" value="LOGIN"/>
        </form>
      </div>
    </div>
  )
}

export default LoginPage;
