import React from 'react';
import Header from './Header';
import PackItem from './PackItem';
import '../styles/homePage.css';
import response from '../response';
import { useHistory } from 'react-router-dom';

function HomePage(props){
  const packList = response.data.pack_list;
  let history = useHistory();
  const handleOnClick = (e,packId) => {
    history.push(`/${packId}/purchase`);
  }
  return(
    <>
      <Header/>
      <div className='flex-grid'>
        {
          packList.map(item =>
            <PackItem
              key={item.pack_id}
              item={item}
              handleOnClick={handleOnClick}
            />
          )
        }
      </div>
    </>
  )
}

export default HomePage;