import React from 'react';

function PackClassCircle({alias}){
  const getCircleValueFromAlias = (alias) =>{
    switch(true){
      case alias.includes('single-pack'): return 'S';
      case alias.includes('pack-10'): return 10;
      case alias.includes('pack-20'): return 20;
      case alias.includes('pack-30'): return 30;
      case alias.includes('pack-50'): return 50;
      case alias.includes('unlimited-pack'): return '∞';
      default: return null;
    }
  }
  return(
    <span style={{borderRadius: '50%',border: '1px solid teal',padding: '1px'}}>
      <p style={{
        borderRadius: '50%',
        color: 'white',
        backgroundColor: '#80CBC4',
        padding: '10px',
        margin: 'auto',
        textAlign: 'center',
        width: '15px',
        height: '15px'
      }}>
        {getCircleValueFromAlias(alias)}
      </p>
    </span>
  )
}

export default PackClassCircle;