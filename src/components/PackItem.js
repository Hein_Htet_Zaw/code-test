import React from 'react';
import PackClassCircle from './PackClassCircle';
import '../styles/packItem.css';
import bgImage from '../assets/dummy-img.jpg';

function PackItem(props){
  const {item} = props;

  const getMeteredPriceFromAlias = (alias,price) =>{
    switch(true){
      case alias.includes('single-pack'): return null;
      case alias.includes('pack-10'): return `$${Math.floor(price/10)} per class!`;
      case alias.includes('pack-20'): return `$${Math.floor(price/20)} per class!`;
      case alias.includes('pack-30'): return `$${Math.floor(price/30)} per class!`;
      case alias.includes('pack-50'): return `$${Math.floor(price/50)} per class!`;
      case alias.includes('unlimited-pack'): return 'per month';
      default: return null;
    }
  }
  return(
    <div className='col' onClick={(e)=>props.handleOnClick(e,item.pack_id)}>
      {
        item.tag_name &&
          <span className='tag-name'>
              POPULAR
          </span>
      }
      <div
        className='crop-image'
        style={{
          backgroundImage: `url(${bgImage})`,
        }}
      >
      </div>
      <div className='main-content'>
        <p className='pack-name'>
          {item.pack_name}
        </p>

        <PackClassCircle alias={item.pack_alias}/>

        <p className='pack-desc'>
          {item.pack_description}
        </p>

        <div className='price-section'>
          <p className='price'>
            {
              item.pack_alias.includes('unlimited-pack') ?
                `$${(item.pack_price/12).toFixed(2)}`
              :
                `$${item.pack_price.toFixed(2)}`
            }
          </p>
          {
            getMeteredPriceFromAlias(item.pack_alias,item.pack_price) &&
              <p className='metered-price'>
                {getMeteredPriceFromAlias(item.pack_alias,item.pack_price)}
              </p>
          }
        </div>
      </div>
    </div>
  )
}



export default PackItem;