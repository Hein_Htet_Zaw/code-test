import React from 'react';
import { useAuth } from "../context/auth";
import {useHistory} from 'react-router-dom';

const Header = (props) => {
  const {setLoggedInUser} = useAuth();
  let history = useHistory();

  const handleLogout = ()=>{
    window.localStorage.removeItem('user');
    setLoggedInUser({isLoggedIn: false})
  }
  return (
    <div className="header">
      <h2 onClick={()=> history.push('/')}>Code-Test</h2>
      <button onClick={handleLogout}>LOGOUT</button>
    </div>
  )
}

export default Header;