import React from 'react';
import { Route, Redirect } from "react-router-dom";
import { useAuth } from "./context/auth";

function PrivateRoute({ component: Component, ...rest }) {
  const { loggedInUser } = useAuth();

  return(
    <Route {...rest} render={(props) => (
      loggedInUser.isLoggedIn ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{ pathname: "/login"}}
        />
      )
    )}
    />
  );
}

export default PrivateRoute;
