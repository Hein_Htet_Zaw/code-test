import React,{useState} from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import PrivateRoute from './PrivateRoute';
import { AuthContext } from "./context/auth";

import LoginPage from '../src/components/LoginPage';
import HomePage from '../src/components/HomePage';
import PurchasePage from '../src/components/PurchasePage';

function App() {
  const [loggedInUser, setLocalUser] = useState(JSON.parse(localStorage.getItem('user')) || {isLoggedIn: false});
  const setLoggedInUser = (user) => {
    localStorage.setItem("user", JSON.stringify(user));
    setLocalUser(user)
  }


  return (
    <AuthContext.Provider value={{loggedInUser,setLoggedInUser}}>
      <Router>
        <Switch>
          <Route exact path="/login" component={LoginPage} />
          <PrivateRoute exact path="/" component={HomePage} />
          <PrivateRoute exact path="/:packId/purchase" component={PurchasePage} />
        </Switch>
      </Router>
    </AuthContext.Provider>
  );
}

export default App;
